import 'react-app-polyfill/ie11';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { App } from '../.';

const App = () => {
  return (
    <div>
      <App />
    </div>
  );
};

ReactDOM.render(<App />, document.getElementById('root'));
