# code-challenge-react-typescript

The purpose of this challenge is to assess your skills with React and Typescript. You will be creating a re-usable component that fulfills a single purpose. This should take about 2 - 3 hours.

## Getting Started

First, clone this repository. Then, edit the `App` component, in `src/index.tsx`, to fulfill the requirements. You may create new components and organize the codebase, as you see fit, as long as `<App />` is something that meets the requirements. You may install any utility libraries that you like to use (for example `axios` and `classnames`), but no "component library" or CSS framework dependencies.

## Requirements

- Search form with field that accepts the user's input.
- Either a submit button *or* "search as you type" functionality.
- Usage of a remote data source.
- Present the fist 10 results, below the search form.
- Display results in a manner that is logical, for the chosen data source.

## Remote Data Source

The component should communicate with a remote API. You may use the listed APIs or use a _public_ API of your choice.

- Internet Archive: `https://archive.org/advancedsearch.php?output=json&q=subject:${SEARCH}`
- [Open Library](https://openlibrary.org/dev/docs/api/search): http://openlibrary.org/search.json?q=${SEARCH}

## Evaluation Rubric

We will focus the evaluation on the following aspects of the final submission.

- Usability and user experience
- Usage of typescript
- Implementation of remote data source
- Implementation of form, input, and form submission
- Implementation of styles
- Modularity and Re-usability of code base
